%global moduledir %(pkg-config xorg-server --variable=moduledir )
%global driverdir %{moduledir}/drivers

%undefine _hardened_build

Name:           xorg-x11-drv-vesa
Version:        2.6.0
Release:        1
Summary:        Xorg X11 vesa video driver
License:        MIT
URL:            https://www.x.org
Source0:        https://xorg.freedesktop.org/releases/individual/driver/xf86-video-vesa-%{version}.tar.xz

ExclusiveArch:  %{ix86} x86_64

BuildRequires:  xorg-x11-server-devel >= 1.10.99.902
BuildRequires:  autoconf automake libtool make

Requires:       Xorg %([ -e /usr/bin/xserver-sdk-abi-requires ] && xserver-sdk-abi-requires ansic)
Requires:       Xorg %([ -e /usr/bin/xserver-sdk-abi-requires ] && xserver-sdk-abi-requires videodrv)
Requires:       xorg-x11-server-wrapper

%description 
X.Org X11 vesa video driver.

%package_help

%prep
%autosetup -n xf86-video-vesa-%{version} -p1

%build
autoreconf -v --install || exit 1
%configure
%make_build

%install
%make_install

%delete_la_and_a

%files
%defattr(-,root,root)
%license COPYING
%{driverdir}/vesa_drv.so

%files help
%defattr(-,root,root)
%doc README.md ChangeLog
%{_mandir}/man4/vesa.4*

%changelog
* Mon Jan 30 2023 zhouyihang <zhouyihang3@h-partners.com> - 2.6.0-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update xorg-x11-drv-vesa to 2.6.0

* Mon Oct 24 2022 zhouyihang <zhouyihang3@h-partners.com> - 2.5.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix spelling wording issues

* Sat Jan 30 2021 xihaochen <xihaochen@huawei.com> - 2.5.0-1
- Type:requirements                                                                                                                                  
- Id:NA
- SUG:NA
- DESC:update xorg-x11-drv-vesa to 2.5.0

* Sat Mar 21 2020 fengtao <fengtao40@huawei.com> - 2.4.0-2
- Refuse vesa on UEFI

* Tue Mar 10 2020 hexiujun <hexiujun1@huawei.com> - 2.4.0-1
- Package init
